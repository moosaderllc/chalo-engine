# Chalo Engine

## Modules

* Base
* Managers
    * Window Manager
    * Config Manager
    * Language Manager
    * Menu Manager
    * State Manager
    * Script Manager (parser)
    * Input Manager
    * Game Object Manager
    * Asset Managers
        * Fonts
        * Images
        * Sounds
* Input devices
    * Mouse
    * Keyboard
    * Gamepad
* GameObjects
    * Properties
        * Animated
        * Top-down movement
        * Platformer movement
        * Rotation movement
        * Rectangle Collision
        * Radius Collision
    * Base Game Object
    * Map Tile Object
    * Character Object
* UI
    * Base Widget
    * Image
    * Label
    * Button (BG Image + Icon Image + Label)
    * TextBox
* Utilities
    * Logger
    * Platform Info
    * String Utilities
    * Messager (send global info)
* Accessibility
    * Controller Mapping (via Input Device objects)
    * Font for Dyslexia
    * Color Blindness (Figure out how to swap palettes?)
    * Text to Speech (need a 3rd party library, or screen reader integration)
    * Menu scaling / text scaling
    * Subtitles
    
Script files for...

* Menu building
* Language
* Maps
* Config

## Notes

I hate starting a new project and having to deal with the language
settings and more complicated things right off the bat; these
should have defaults they work with, without any futzing around.

## Boilerplate

Maybe have basic Sound / Music volume config options baked in.

Try to have as many accessibility things baked in.

## Modular game objects...

Would be cool to define like...

```
player = Character
                with Animation
                    using 32 as frame-width
                    using 32 as frame-height
                    using ROW 0 as north
                    using ROW 1 as south
                    using ROW 2 as west
                    using ROW 3 as east
                    using 4 as max-frames
                with BoundingBoxCollision
                with TopDownMovement
                with KeyboardInput
                    using SPACE as jump
                    using LEFT as left
                    using RIGHT as right
                    ...
```
